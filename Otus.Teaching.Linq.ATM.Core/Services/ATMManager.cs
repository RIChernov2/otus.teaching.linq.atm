﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;


namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата

        public void GetUserInfo(string login, string password)
        {
            User user = GetUserOrNull(login, password);
            if ( !CheckUserAndTypeAboutFailure(user) ) return;

            user.Print();
        }
        public void GetAccountsInfo(string login, string password)
        {
            User user = GetUserOrNull(login, password);
            if ( !CheckUserAndTypeAboutFailure(user) ) return;

            IEnumerable<Account> userAccounts = GetUserAccountsOrNull(user);
            if ( !CheckUserAccountsAndTypeAboutFailure(userAccounts) ) return;

            System.Console.WriteLine("Счета пользователя:");
            foreach ( Account account in userAccounts )
            {
                account.Print();
            }
        }
        public void GetAccountsWithHistoryInfo(string login, string password)
        {
            User user = GetUserOrNull(login, password);
            if ( !CheckUserAndTypeAboutFailure(user) ) return;

            IEnumerable<Account> userAccounts = GetUserAccountsOrNull(user);
            if ( !CheckUserAccountsAndTypeAboutFailure(userAccounts) ) return;

            var accountsWithHistory = from userAccount in userAccounts
                                     join operationHistory in History
                                     on userAccount.Id equals operationHistory.AccountId into historyGroup
                                     select new { Account = userAccount,History = historyGroup };

            foreach ( var accWithHistory in accountsWithHistory )
            {
                System.Console.WriteLine("Счет:");
                System.Console.WriteLine(accWithHistory.Account);

                System.Console.WriteLine("История счета:");
                if ( accWithHistory.History.Count() == 0 )
                {
                    System.Console.WriteLine("- нет данных об операция по данному счету.");
                }
                else
                {
                    foreach ( OperationsHistory operation in accWithHistory.History )
                    {
                        operation.Print();
                    }
                }
                System.Console.WriteLine("");
            }
        }
        public void TypeInfoAboutAllIncomeOperations()
        {
            var incomInfo = from user in Users
                            join opWithUserId in
                               from account in Accounts
                               join incomeOperation in
                                   from operaton in History
                                   where operaton.OperationType == OperationType.InputCash
                                   select operaton
                               on account.Id equals incomeOperation.Id
                               select new { UserId = account.UserId, incomeOperation = incomeOperation }
                            on user.Id equals opWithUserId.UserId into Operations
                            select new { User = user, Operations = Operations };


            System.Console.WriteLine("Операции пополнения счетов:\n");

            foreach ( var item in incomInfo )
            {
                System.Console.WriteLine($"ФИО: {item.User.FirstName} {item.User.SurName} {item.User.MiddleName}");
                foreach ( var operationWithId in item.Operations )
                {
                    System.Console.WriteLine($"Операция: {operationWithId.incomeOperation}");

                }
                System.Console.WriteLine("");
            }
        }
        public void TypeUsersWithAccountWithCashMoreThenInput(decimal cash)
        {

            var usersData = (from userAccs in
                                from acc in Accounts
                                where acc.CashAll > cash
                                select acc
                            join user in Users
                            on userAccs.UserId equals user.Id
                            select user).Distinct().OrderBy(user=> user.SurName);

            if ( usersData.Count() == 0 )
            {
                System.Console.WriteLine($"Нет пользователей с сумой на счету большей чем {cash}.");
                return;
            }

            System.Console.WriteLine($"Пользователи с сумой на счету большей чем {cash}:");
            foreach ( var user in usersData )
            {
                System.Console.WriteLine($"ФИО: {user.FirstName} {user.SurName} {user.MiddleName}");
            }
        }


        private User GetUserOrNull(string login, string password)
        {         
            return Users.Where(x => x.Login == login && x.Password == password).FirstOrDefault(); ;
        }
        private bool CheckUserAndTypeAboutFailure(User user)
        {
            if ( user != null )
            {
                return true;
            }
            else
            {
                System.Console.WriteLine("Неверный логин или пароль.");
                return false;
            }
        }
        private IEnumerable<Account> GetUserAccountsOrNull(User user)
        {
            return Accounts.Where(x => x.UserId == user.Id);
        }
        private bool CheckUserAccountsAndTypeAboutFailure(IEnumerable<Account> accounts)
        {
            if ( accounts != null )
            {
                return true;
            }
            else
            {
                System.Console.WriteLine("На пользователя не зарегистрировано счетов");
                return false;
            }
        }
    }
}