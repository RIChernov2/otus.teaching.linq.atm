﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();


            //TODO: Далее выводим результаты разработанных LINQ запросов

            // 1.Информация о пользователе.
            System.Console.WriteLine("1. Информация о пользователе.\n");

            atmManager.GetUserInfo("Snow", "111");
            System.Console.WriteLine("");

            atmManager.GetUserInfo("snow", "111");
            System.Console.WriteLine("");

            // 2. Информация по счетам.
            System.Console.WriteLine("2. Информация по счетам.\n");

            atmManager.GetAccountsInfo("login", "password");
            System.Console.WriteLine("");

            atmManager.GetAccountsInfo("lee", "222");
            System.Console.WriteLine("");

            // 3. Информация по счетам с историей.
            System.Console.WriteLine("3. Информация по счетам с историей.\n");

            atmManager.GetAccountsInfo("login", "password");
            System.Console.WriteLine("");

            atmManager.GetAccountsWithHistoryInfo("cant", "333");
            System.Console.WriteLine("");

            atmManager.GetAccountsWithHistoryInfo("cop", "555");
            System.Console.WriteLine("");

            // 4. Данные об операциях пополнения счета.
            System.Console.WriteLine("4. Данные об операциях пополнения счета.\n");
            atmManager.TypeInfoAboutAllIncomeOperations();

            // 5. Пользователи с суммой больше чем.
            System.Console.WriteLine("5.Пользователи с суммой больше чем.\n");
            atmManager.TypeUsersWithAccountWithCashMoreThenInput(1111111);
            System.Console.WriteLine("");

            atmManager.TypeUsersWithAccountWithCashMoreThenInput(10);
            System.Console.WriteLine("");

            atmManager.TypeUsersWithAccountWithCashMoreThenInput(10001);
            System.Console.WriteLine("");



            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}